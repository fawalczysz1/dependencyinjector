This project is a custom dependency injector built in and for Java 8+

It contains multiple features:
	-	Field injection
	-	Setter injection
	-	Constructor injection
	-	Singleton injection
	-	Lazy initialization prototype
	
And handles multiple Objects:
	-	Primitives
	-	Classes and its parents
	-	Interfaces
	-	Abstract classes

Cyclic injection is handled throwing ImplementationException

Each method is detailed inside Javadoc