package fr.isima.dependency.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.isima.dependency.classes.SingletonClass;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.injector.Injector;

public class SingletonInjectorTest {

	@Test
	public void testSingletonInjection() throws ImplementationException, ClassNotFoundException {
		final SingletonClass singletonClass = Injector.inject(SingletonClass.class);
		assertEquals("simpleValue", singletonClass.getSimpleValue());
		assertEquals("parentA", singletonClass.getParentValue());

		final SingletonClass singletonClass2 = Injector.inject(SingletonClass.class);
		assertEquals("simpleValue", singletonClass2.getSimpleValue());
		assertEquals("parentA", singletonClass2.getParentValue());

		assertEquals(1, SingletonClass.getCounter());
		assertEquals(singletonClass, singletonClass2);
	}

}
