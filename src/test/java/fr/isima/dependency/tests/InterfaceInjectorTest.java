package fr.isima.dependency.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.isima.dependency.abstracts.AAbstractClass;
import fr.isima.dependency.classes.BasicClass;
import fr.isima.dependency.classes.InterfaceClass;
import fr.isima.dependency.classes.InterfaceHolder;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.implementations.AbstractClass;
import fr.isima.dependency.implementations.FakeClass2;
import fr.isima.dependency.implementations.Injected;
import fr.isima.dependency.injector.Injector;
import fr.isima.dependency.interfaces.IFakeClass;
import fr.isima.dependency.interfaces.IInjected;
import fr.isima.dependency.interfaces.INoImplem;

public class InterfaceInjectorTest {

	@Test
	public void testInterfaceInjection() throws ImplementationException, ClassNotFoundException {
		final Object interfaceClass = Injector.inject(InterfaceClass.class);
		assertEquals(InterfaceClass.class, interfaceClass.getClass());
	}

	@Test
	public void testInterfaceHolderInjection() throws ClassNotFoundException, ImplementationException {
		final InterfaceHolder interfaceHolder = Injector.inject(InterfaceHolder.class);
		assertEquals(FakeClass2.class, interfaceHolder.fakeClass.getClass());
		assertEquals(Injected.class, interfaceHolder.injected.getClass());
	}

	@Test
	public void testInterfaceMethodInjection() throws ClassNotFoundException, ImplementationException {
		final IInjected bclass = Injector.inject(IInjected.class);
		assertEquals("done", bclass.todo());
	}

	@Test(expected = ImplementationException.class)
	public void testInterfaceNoImplem() throws ClassNotFoundException, InstantiationException {
		Injector.inject(INoImplem.class);
		fail("No error occured");
	}

	@Test
	public void testInjectSpecificAbstractClass() throws ClassNotFoundException, InstantiationException {
		final AAbstractClass aClass = Injector.injectSpecificInterface(AAbstractClass.class, AbstractClass.class);
		assertEquals("test2", aClass.getTest());
	}

	@Test
	public void testInjectSpecificInterface() throws ClassNotFoundException, InstantiationException {
		IFakeClass fakeClass;
		fakeClass = Injector.injectSpecificInterface(IFakeClass.class, FakeClass2.class);
		assertEquals("FakeClass2", fakeClass.toString());
	}

	@Test(expected = InstantiationException.class)
	public void testInjectBadImplementation() throws ClassNotFoundException, InstantiationException {
		Injector.injectSpecificInterface(BasicClass.class, FakeClass2.class);
		fail("No error occured");

	}

	@Test(expected = InstantiationException.class)
	public void testMultipleInterface() throws ClassNotFoundException, InstantiationException {
		Injector.inject(IFakeClass.class);
		fail("No error occured");
	}

}
