package fr.isima.dependency.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.isima.dependency.classes.ConstructorInjectionClass;
import fr.isima.dependency.classes.MultipleConstructorInjection;
import fr.isima.dependency.classes.NoConstructor;
import fr.isima.dependency.classes.PrivateConstructorClass;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.injector.Injector;

public class ConstructorInjectorTest {

	@Test
	public void testConstructorInjection() throws ImplementationException, ClassNotFoundException {
		final ConstructorInjectionClass ciclass = Injector.constructorInjection(ConstructorInjectionClass.class);
		assertEquals("simpleValue", ciclass.getSimpleValue());
		assertEquals(0, ciclass.getByt());
		assertEquals(0, ciclass.getDoubl(), 0);
		assertEquals(0, ciclass.getFloa(), 0);
		assertEquals(0, ciclass.getIn());
		assertEquals(0, ciclass.getLon());
		assertEquals(0, ciclass.getShor());
		assertEquals("", ciclass.getStr());
		assertEquals('\u0000', ciclass.getChr());
		assertEquals(false, ciclass.isBool());
	}

	@Test(expected = ImplementationException.class)
	public void testMultipleConstructorInjection() throws ImplementationException, ClassNotFoundException {
		Injector.constructorInjection(MultipleConstructorInjection.class);
		fail("No error occured");
	}

	@Test
	public void testPrivateConstructorInjection() throws ImplementationException, ClassNotFoundException {
		final PrivateConstructorClass privateConstructorClass = Injector.inject(PrivateConstructorClass.class);
		assertNotNull(privateConstructorClass.getSimpleClass());
	}

	@Test(expected = ImplementationException.class)
	public void testNoConstructorInjection() throws ClassNotFoundException, InstantiationException {
		Injector.constructorInjection(NoConstructor.class);
		fail("No error occured");
	}

}
