package fr.isima.dependency.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.isima.dependency.classes.BasicClass;
import fr.isima.dependency.classes.SimpleClass;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.injector.Injector;

public class ClassInjectorTest {

	@Test
	public void testObjectInjection() throws ImplementationException, ClassNotFoundException {
		final SimpleClass val = Injector.inject(SimpleClass.class);
		assertEquals("simpleValue", val.getValue());
	}

	@Test
	public void testDefaultObjectInjection() throws ImplementationException, ClassNotFoundException {
		final SimpleClass val = Injector.inject(SimpleClass.class);
		assertEquals("", val.getDefaultValue());
	}

	@Test
	public void testClassInjection() throws ClassNotFoundException, ImplementationException {
		final BasicClass bclass = Injector.inject(BasicClass.class);
		assertEquals("a", bclass.getAValue());
	}

	@Test
	public void testCounter() throws ImplementationException, ClassNotFoundException {
		BasicClass.resetCounter();
		Injector.inject(BasicClass.class);
		Injector.inject(BasicClass.class);
		Injector.inject(BasicClass.class);
		assertEquals(3, BasicClass.counter);
	}

}
