package fr.isima.dependency.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.isima.dependency.abstracts.AAbstractClass;
import fr.isima.dependency.abstracts.AAbstractMultiple;
import fr.isima.dependency.abstracts.AAbstractNoImplem;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.injector.Injector;

public class AbstractInjectorTest {

	@Test
	public void testAbstractInjection() throws ImplementationException, ClassNotFoundException {

		final AAbstractClass aaclass = Injector.inject(AAbstractClass.class);
		assertEquals("test2", aaclass.getTest());
	}

	@Test(expected = ImplementationException.class)
	public void testAbstractMultipleInjection() throws ImplementationException, ClassNotFoundException {
		final AAbstractMultiple aaclass = Injector.inject(AAbstractMultiple.class);
		fail("No error occured");
	}

	@Test(expected = ImplementationException.class)
	public void testAbstractNoInjection() throws ImplementationException, ClassNotFoundException {
		final AAbstractNoImplem aaclass = Injector.inject(AAbstractNoImplem.class);
		fail("No error occured");

	}
}
