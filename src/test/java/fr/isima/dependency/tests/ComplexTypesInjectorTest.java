package fr.isima.dependency.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.isima.dependency.classes.A;
import fr.isima.dependency.classes.CycleA;
import fr.isima.dependency.classes.InjectionCycleA;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.injector.Injector;

public class ComplexTypesInjectorTest {

	@Test
	public void testParentInjection() throws ClassNotFoundException, ImplementationException {
		final A a = Injector.inject(A.class);
		assertEquals("a", a.getValue());
	}

	@Test
	public void RecursiveParentInjection() throws ClassNotFoundException, ImplementationException {
		final A a = Injector.inject(A.class);
		assertEquals("parentA", a.getParentValue());
		assertEquals("parentAA", a.getParentAAValue());
	}

	@Test
	public void testCycleInjection() throws ImplementationException, ClassNotFoundException {

		final CycleA cycleA = Injector.inject(CycleA.class);
		assertEquals(true, cycleA.recycle());
	}

	@Test(expected = Exception.class)
	public void testCyclicInjection() throws ImplementationException, ClassNotFoundException {
		final InjectionCycleA cycleA = Injector.inject(InjectionCycleA.class);

	}
}
