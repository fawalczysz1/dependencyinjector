package fr.isima.dependency.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import fr.isima.dependency.classes.LazyInjectorClass;
import fr.isima.dependency.classes.SetterInjectionClass;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.injector.Injector;
import fr.isima.dependency.lazyinjector.LazyInjector;

public class SetterInjectorTest {

	@Test
	public void testLazyInjector()
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, InterruptedException,
			NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		final LazyInjectorClass noImplem = LazyInjector.lazyInjectMethod(LazyInjectorClass.class);
		assertEquals("test", noImplem.getValue());
		assertEquals("", noImplem.getNullValue());
		assertEquals(0, noImplem.getVal());
		assertNotNull(noImplem.getA());

	}

	@Test
	public void testSetterInjection() throws ImplementationException, ClassNotFoundException {
		final SetterInjectionClass setterInjectionClass = Injector.setterInjection(SetterInjectionClass.class);
		assertEquals("a", setterInjectionClass.getA().getValue());
		assertEquals("parentA", setterInjectionClass.getA().getParentValue());
		assertEquals("simpleValue", setterInjectionClass.getSimpleClass().getValue());
	}

}
