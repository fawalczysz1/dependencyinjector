package fr.isima.dependency.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.injector.Injector;

/**
 * Unit test for simple App.
 */
public class InjectorTest {

	@Test
	public void testStringInjection() throws ClassNotFoundException, ImplementationException {
		final String a = Injector.inject(String.class);
		assertEquals("", a.toString());
	}

	@Test
	public void testIntInjection() throws ImplementationException, ClassNotFoundException {
		final int val = Injector.inject(int.class);
		assertEquals(0, val);
	}

}
