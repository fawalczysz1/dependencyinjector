package fr.isima.dependency.implementations;

import fr.isima.dependency.annotations.Implements;
import fr.isima.dependency.interfaces.IFakeClass;

@Implements({IFakeClass.class})
public class FakeClass2 implements IFakeClass {

	public String toString() {
		return "FakeClass2";
	}
	
}
