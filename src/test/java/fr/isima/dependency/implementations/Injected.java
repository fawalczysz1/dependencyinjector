package fr.isima.dependency.implementations;

import fr.isima.dependency.annotations.Implements;
import fr.isima.dependency.annotations.Singleton;
import fr.isima.dependency.interfaces.IInjected;

@Singleton
@Implements(IInjected.class)
public class Injected implements IInjected {

	@Override
	public String todo() {
		return "done";
	}

}
