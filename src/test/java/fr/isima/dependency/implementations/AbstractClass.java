package fr.isima.dependency.implementations;

import fr.isima.dependency.abstracts.AAbstractClass;
import fr.isima.dependency.annotations.Abstract;

@Abstract({AAbstractClass.class})
public class AbstractClass extends AAbstractClass {

	private String test ="test2";
	
	
	public String getSuperTest() {
		return super.getTest();
	}
	
	
	public String getTest() {
		return test;
	}
	
}
