package fr.isima.dependency.implementations;

import fr.isima.dependency.annotations.Implements;
import fr.isima.dependency.interfaces.IFakeClass;

@Implements({IFakeClass.class})
public class FakeClass implements IFakeClass {

	public String toString() {
		return "FakeClass";
	}
	
}
