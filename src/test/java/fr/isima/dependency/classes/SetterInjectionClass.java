package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectSetter;

public class SetterInjectionClass {

	private SimpleClass simpleClass;

	private A a;

	public SimpleClass getSimpleClass() {
		return simpleClass;
	}

	@InjectSetter
	public void setSimpleClass(final SimpleClass simpleClass) {
		this.simpleClass = simpleClass;
	}

	public A getA() {
		return a;
	}

	@InjectSetter
	public void setA(final A a) {
		this.a = a;
	}

}
