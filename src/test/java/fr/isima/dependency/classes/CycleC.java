package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;

public class CycleC {

	@InjectField
	CycleD cycleD;
	
	public boolean recycle() {
		return cycleD.recycle();
	}
}
