package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;
import fr.isima.dependency.implementations.FakeClass2;
import fr.isima.dependency.interfaces.IFakeClass;
import fr.isima.dependency.interfaces.IInjected;

public class InterfaceHolder {

	@InjectField(FakeClass2.class)
	public IFakeClass fakeClass;

	@InjectField
	public IInjected injected;

}
