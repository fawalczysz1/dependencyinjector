package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;

public class A extends ParentA {

	@InjectField
	private final String value = "a";

	public String getValue() {
		return value;
	}

	public String getParentValue() {
		return parentValue;
	}

	@Override
	public String getParentAAValue() {
		return super.getParentAAValue();
	}

}
