package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;

public class PrivateConstructorClass {

	@InjectField
	private SimpleClass simpleClass;

	private PrivateConstructorClass() {

	}

	public SimpleClass getSimpleClass() {
		return simpleClass;
	}

}
