package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;

public class CycleA {
	
	@InjectField
	CycleB cycleB;
	
	@InjectField
	CycleC cycleC;
	
	
	public boolean recycle() {
		return cycleB.recycle() && cycleC.recycle();
	}
}
