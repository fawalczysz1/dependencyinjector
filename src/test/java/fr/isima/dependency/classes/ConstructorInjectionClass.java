package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectConstructor;

public class ConstructorInjectionClass {

	private final SimpleClass simpleClass;

	private int in;

	private String str;

	private boolean bool;

	private byte byt;

	private long lon;

	private float floa;

	private short shor;

	private double doubl;

	private char chr;

	@InjectConstructor
	public ConstructorInjectionClass(final SimpleClass simpleClass, final int in, final String str, final char chr,
			final boolean bool, final byte byt, final long lon, final float floa, final short shor,
			final double doubl) {
		this.simpleClass = simpleClass;
		this.in = in;
		this.str = str;
		this.chr = chr;
		this.bool = bool;
		this.byt = byt;
		this.lon = lon;
		this.floa = floa;
		this.shor = shor;
		this.doubl = doubl;
	}

	public ConstructorInjectionClass(final SimpleClass simpleClass) {
		this.simpleClass = simpleClass;
	}

	public String getSimpleValue() {
		return simpleClass.getValue();
	}

	public String getStr() {
		return str;
	}

	public void setStr(final String str) {
		this.str = str;
	}

	public int getIn() {
		return in;
	}

	public boolean isBool() {
		return bool;
	}

	public byte getByt() {
		return byt;
	}

	public long getLon() {
		return lon;
	}

	public float getFloa() {
		return floa;
	}

	public short getShor() {
		return shor;
	}

	public double getDoubl() {
		return doubl;
	}

	public char getChr() {
		return chr;
	}

}
