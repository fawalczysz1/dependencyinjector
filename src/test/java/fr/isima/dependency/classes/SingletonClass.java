package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;
import fr.isima.dependency.annotations.Singleton;

@Singleton
public class SingletonClass {

	public static int counter = 0;

	@InjectField
	private SimpleClass simpleClass;

	@InjectField
	private A a;

	public SingletonClass() {
		counter++;
	}

	public String getSimpleValue() {
		return simpleClass.getValue();
	}

	public String getParentValue() {
		return a.getParentValue();
	}

	public static int getCounter() {
		return counter;
	}

}
