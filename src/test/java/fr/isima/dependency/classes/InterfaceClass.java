package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;
import fr.isima.dependency.implementations.FakeClass2;
import fr.isima.dependency.interfaces.IFakeClass;

public class InterfaceClass {

	@InjectField(FakeClass2.class)
	IFakeClass ifakeClass ;
	
	public String getValue() {
		return ifakeClass.toString();
	}
	
}
