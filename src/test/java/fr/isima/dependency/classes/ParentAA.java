package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;

public class ParentAA {
	@InjectField
	protected String parentParentValue = "parentAA";

	protected String getParentAAValue() {
		return parentParentValue;
	}
}
