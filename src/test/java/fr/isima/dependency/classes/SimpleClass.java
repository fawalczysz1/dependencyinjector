package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;

public class SimpleClass {
	@InjectField
	private final String value = "simpleValue";

	@InjectField
	private String defaultValue;

	public String getValue() {
		return value;
	}

	public String getDefaultValue() {
		return defaultValue;
	}
}
