package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;

public class CycleD {

	@InjectField
	CycleE cycleE;
	
	public boolean recycle() {
		return cycleE.recycle();
	}
}
