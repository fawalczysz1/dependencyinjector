package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;

public class BasicClass {

	@InjectField
	private A a;

	@InjectField
	private SimpleClass simpleClass;

	public static int counter = 0;

	public BasicClass() {
		counter++;
	}

	public static void resetCounter() {
		counter = 0;
	}

	public String getAValue() {
		return a.getValue();
	}

	public String getSimpleClassValue() {
		return simpleClass.getValue();
	}

}
