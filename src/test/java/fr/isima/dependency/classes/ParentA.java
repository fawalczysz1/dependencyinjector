package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectField;

public class ParentA extends ParentAA {
	@InjectField
	protected String parentValue = "parentA";

	@Override
	protected String getParentAAValue() {
		return super.getParentAAValue();
	}

}
