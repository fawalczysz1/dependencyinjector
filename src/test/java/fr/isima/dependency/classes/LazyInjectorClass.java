package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.Lazy;
import fr.isima.dependency.lazyinjector.ILazyInjector;

@SuppressWarnings("unused")
public class LazyInjectorClass implements ILazyInjector{

	private String value = "test";
	private String nullValue;
	private int val;
	private A a;
	
	
	public int getVal() {
		return get("val");
	}
	@Lazy
	public void setVal(int val) {
		this.val = val;
	}

	public A getA() {
		return get("a");
	}
	@Lazy
	public void setA(A a) {
		this.a = a;
	}
	
	public String getValue() {
		return get("value");
	}
	
	@Lazy
	public void setValue(String value) {
		this.value=value;
	}
	
	public String getNullValue() {
		return get("nullValue");
	}
	
	@Lazy
	public void setNullValue(String value) {
		this.nullValue=value;
	}

	
	
}
