package fr.isima.dependency.classes;

import fr.isima.dependency.annotations.InjectConstructor;

public class MultipleConstructorInjection {

	@InjectConstructor 
	public MultipleConstructorInjection(String str) {
		
	}

	@InjectConstructor 
	public MultipleConstructorInjection(String str, int val) {
		
	}
}
