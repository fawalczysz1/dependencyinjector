package fr.isima.dependency.injector;

import java.util.List;
import java.util.Set;

import org.reflections.Reflections;

import fr.isima.dependency.annotations.Abstract;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.utils.Utils;

final class AbstractInjector {

	private AbstractInjector() {

	}

	/**
	 * Specific treatment for abstract class injection. Search for the class
	 * implementing the given interface and inject it. This search is based on
	 * annotation {@link fr.isima.dependency.annotations.Abstract @Abstract}
	 * 
	 * @param clazz
	 *            Abstract class to process
	 * @return The injected class
	 * @throws ImplementationException
	 *             No implementation were found for the given abstract class
	 * @throws ClassNotFoundException
	 *             No annotation found for the given abstract class
	 */
	@SuppressWarnings("unchecked")
	static <T> T abstractInjection(final Class<T> clazz, final List<Class<?>> forbidden)
			throws ImplementationException, ClassNotFoundException {
		final Reflections reflections = new Reflections();
		// Gather classes that implements our interface
		final Set<Class<?>> implementers = reflections.getTypesAnnotatedWith(Abstract.class, true);
		T foundImplementation = null;
		Class<T> foundClassImplementation = null;

		// get all classes containing Implements annotation
		for (final Class<?> implementer : implementers) {
			// if this class implements clazz
			if (Utils.contains(implementer.getAnnotation(Abstract.class).value(), clazz)) {
				if (foundClassImplementation != null)
					throw new ImplementationException(
							"Multiple implementations found for abstract class " + clazz.getName());
				foundClassImplementation = (Class<T>) implementer; // Inject found implementation and returns it
			}
		}

		if (foundClassImplementation == null)
			throw new ImplementationException("No implementations were found for abstract class: " + clazz.getName());

		foundImplementation = Injector.inject(foundClassImplementation, forbidden);

		return foundImplementation;
	}

}
