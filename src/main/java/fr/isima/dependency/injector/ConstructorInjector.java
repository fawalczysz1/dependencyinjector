package fr.isima.dependency.injector;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import fr.isima.dependency.annotations.InjectConstructor;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.utils.Utils;

final class ConstructorInjector {

	private ConstructorInjector() {

	}

	/**
	 * Constructor injection. Looks for constructor annotated with
	 * {@link fr.isima.dependency.annotations.InjectConstructor InjectConstructor}
	 * and instantiate instance with default values
	 * <p>
	 * if no constructors or multiple ones are annotated inside of the same class
	 * 
	 * @param clazz
	 * @return Instantiated object
	 * @throws ImplementationException
	 */
	@SuppressWarnings("unchecked")
	static <T> T constructorInjection(final Class<T> clazz, final List<Class<?>> forbidden)
			throws ImplementationException {

		// get all constructors from class
		final Constructor<?>[] constructors = clazz.getConstructors();
		T object = null;
		Constructor<?> selectedConstructor = null;
		for (final Constructor<?> constructor : constructors) {
			// get constructors with annotation
			if (constructor.getAnnotation(InjectConstructor.class) != null) {
				if (selectedConstructor != null)
					throw new ImplementationException("Multiple constructors found for class " + clazz.getName());
				selectedConstructor = constructor;
			}
		}
		if (selectedConstructor == null)
			throw new ImplementationException("No constructors were found for class: " + clazz.getName());

		object = (T) instantiateConstructor(selectedConstructor, forbidden);

		return object;
	}

	/**
	 * Instantiate constructor using argument list
	 * 
	 * @param constructor
	 *            to instantiate
	 * @return new Object instantiated using the given constructor
	 */
	static <T> T instantiateConstructor(final Constructor<T> constructor, final List<Class<?>> forbidden) {
		// get all parameters on this constructor
		T object = null;
		final Class<?>[] parameterTypes = constructor.getParameterTypes();
		// initialization of arguments list
		final Object[] args = new Object[parameterTypes.length];
		try {
			// for all parameters of constructor
			for (int i = 0; i < parameterTypes.length; i++) {
				Object obj;
				// get injected parameter
				obj = Injector.inject(parameterTypes[i], forbidden);
				args[i] = obj;
			}
			// create new instance with args list
			object = constructor.newInstance(args);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| ClassNotFoundException e) {
			Utils.error(e.toString());
		}
		return object;
	}

}
