package fr.isima.dependency.injector;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.isima.dependency.annotations.Singleton;
import fr.isima.dependency.exceptions.ImplementationException;

final class InstanceInjector {

	private InstanceInjector() {

	}

	private static Map<Class<?>, Object> singletonMap = new HashMap<>();

	/**
	 * This method allows to get an instance on all classes , even if private
	 * constructor is setted. Classes that annotates
	 * {@link fr.isima.dependency.annotations.Singleton @Singleton} are returned
	 * using rules of singleton injection.
	 * <p>
	 * Classes that cannot be instantiated will return
	 * {@link fr.isima.dependency.exceptions.ImplementationException
	 * ImplementationException}
	 * 
	 * @param clazz
	 *            class to instantiate
	 * @return Instantiated object
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unchecked")
	static <T> T getInstance(final Class<T> clazz) throws InstantiationException {
		Constructor<?> defaultConstructor = null;
		// get of default constructor
		try {
			defaultConstructor = clazz.getDeclaredConstructor((Class<?>[]) null);
		} catch (NoSuchMethodException | SecurityException e) {
			// this should not happen so we return an error
			throw new ImplementationException("class does not contain default Constructor");
		}
		if (defaultConstructor != null) {
			// set accessibility (could be private)
			defaultConstructor.setAccessible(true);
			try {
				// if class is singleton, add its new instance in map
				if (clazz.isAnnotationPresent(Singleton.class)) {
					if (!singletonMap.containsKey(clazz)) {
						singletonMap.put(clazz, defaultConstructor.newInstance());
					}
					// return instantiated reference in map
					return (T) singletonMap.get(clazz);
				} else {
					// return new instance
					return (T) defaultConstructor.newInstance();
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				throw new InstantiationException(e.toString());
			}
		}
		return null;
	}

	/**
	 * Inject class and inherited fields from parent
	 * 
	 * @param clazz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */

	@SuppressWarnings("squid:S4165") // suppress warnings on instance set
	static <T> T classInjection(final Class<T> clazz, List<Class<?>> forbidden)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		// Instantiation of the object
		T newInstance = null;
		try {
			newInstance = Injector.getInstance(clazz);
		} catch (final InstantiationException e) {
			newInstance = Injector.abstractInjection(clazz);
		}
		// injecting class fields
		newInstance = Injector.injectFields(newInstance, clazz, forbidden);
		// Injection of inherited fields
		Class<?> clazz2 = clazz;
		while (clazz2.getSuperclass() != Object.class) {
			clazz2 = clazz2.getSuperclass();
			newInstance = Injector.injectFields(newInstance, clazz2, forbidden);
		}
		return newInstance;
	}

}
