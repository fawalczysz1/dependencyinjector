package fr.isima.dependency.injector;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import fr.isima.dependency.annotations.InjectField;
import fr.isima.dependency.exceptions.ImplementationException;

final class FieldInjector {

	private FieldInjector() {

	}

	/**
	 * Injects all fields from a class into an instance of this class. Fields must
	 * be annotated with {@link fr.isima.dependency.annotations.InjectField @Inject}
	 * in order to be injected
	 * 
	 * If a specific implementation is needed, it can be specified using @Inject
	 * value
	 * 
	 * @param newInstance
	 * @param clazz2
	 * @return injected object
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws ImplementationException
	 */
	@SuppressWarnings("unchecked")
	static <T> T injectFields(final T newInstance, final Class<?> clazz2, final List<Class<?>> forbidden)
			throws IllegalAccessException, ClassNotFoundException, ImplementationException {
		for (final Field field : clazz2.getDeclaredFields()) {
			final InjectField injectField = field.getAnnotation(InjectField.class);
			if (injectField != null) {
				field.setAccessible(true); // Force private fields to be accessible
				final T value = (T) field.get(newInstance);
				T obj = null;

				// Recursive injection of the field
				if (injectField.value() != Object.class) {
					// specified injection field
					obj = (T) Injector.inject(injectField.value(), new ArrayList<>(forbidden));
				} else {
					// basic injection
					obj = (T) Injector.inject(field.getType(), new ArrayList<>(forbidden));
				}
				// Useless to treat differently static and non static because of set behaviour:
				// "If the underlying field is static, the obj argument is ignored; it may be
				// null. "
				if (obj != null && !obj.equals(""))
					field.set(newInstance, obj);
				// if obj contains more than value use obj
				// can happen if string is instantiated with a null value
				else if (obj != null && obj.equals("") && value == null)
					field.set(newInstance, obj);
				else
					field.set(newInstance, value); // If the instantiated object is null, sets the class default value
			}
		}
		return newInstance;
	}

}
