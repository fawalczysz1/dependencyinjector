package fr.isima.dependency.injector;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import fr.isima.dependency.annotations.InjectSetter;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.utils.Utils;

final class SetterInjector {

	private SetterInjector() {

	}

	/**
	 * Setter injection. Gets all setters of class and instantiate all of their
	 * arguments. Method is identified as setter if the method starts with 'set' and
	 * is annotated with {@link fr.isima.dependency.annotations.InjectSetter
	 * InjectSetter}
	 * 
	 * @param clazz
	 *            class to inject
	 * @return injected object
	 * @throws ImplementationException
	 *             if new instance can not be called on it
	 */
	static <T> T setterInjection(final Class<T> clazz, final List<Class<?>> forbidden) throws ImplementationException {
		T newInstance = null;
		try {
			newInstance = Injector.getInstance(clazz);
		} catch (final InstantiationException e) {
			throw new ImplementationException(
					"interfaces, primitives and abstract classes cannot be instatiated using setter injection.");
		}
		final Method[] methods = clazz.getMethods();
		for (final Method method : methods) {
			if (method.isAnnotationPresent(InjectSetter.class) && method.getName().startsWith("set")) {

				try {
					final Object obj = Injector.inject(method.getParameterTypes()[0], forbidden);
					method.invoke(newInstance, obj);

				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| ClassNotFoundException e) {
					Utils.error(e.toString());
				}
			}
		}
		return newInstance;
	}
}
