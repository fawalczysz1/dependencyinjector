package fr.isima.dependency.injector;

import java.util.ArrayList;
import java.util.List;

import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.utils.Utils;

public interface Injector {

	static <T> T inject(final Class<T> clazz) throws ClassNotFoundException, ImplementationException {
		return inject(clazz, new ArrayList<>());
	}

	/**
	 * Basic inject function in order to inject all attributes in a class Classes,
	 * interfaces, inheritance are supported
	 * 
	 * @param <T>
	 * 
	 * @param clazz
	 * @return
	 * @throws ClassNotFoundException
	 * @throws ImplementationException
	 */
	@SuppressWarnings("unchecked")
	static <T> T inject(final Class<T> clazz, List<Class<?>> forbidden)
			throws ClassNotFoundException, ImplementationException {
		T newInstance = null;

		// cycles prevention
		if (forbidden.contains(clazz)) {
			throw new ImplementationException("cyclic injection detected on class" + clazz);
		}
		forbidden.add(clazz);

		try {
			// Interfaces and primitives are processed differently from "classic" classes
			// because they can't be instantiated
			if (!clazz.isInterface() && !clazz.isPrimitive()) {

				newInstance = InstanceInjector.classInjection(clazz, forbidden);
			}
			// interface treatment
			else if (clazz.isInterface()) {
				// Specific process for interface injection

				newInstance = InterfaceInjector.interfaceInjection(clazz, forbidden);
			}
			// primitive treatment
			else {
				newInstance = (T) Utils.getDefaultValue(clazz);
			}

		} catch (final ImplementationException e) {
			throw e;
		} catch (InstantiationException | IllegalAccessException e) {
			// Error thrown when trying to access primitive objects.
			return null;
		} catch (IllegalArgumentException | SecurityException e) {
			Utils.error(e.toString());
		}

		return newInstance;
	}

	/**
	 * @see fr.isima.dependency.injector.SetterInjector #setterInjection(Class)
	 */
	static <T> T setterInjection(final Class<T> clazz) throws ImplementationException {
		return SetterInjector.setterInjection(clazz, new ArrayList<>());
	}

	/**
	 * @see fr.isima.dependency.injector.InstanceInjector #getInstance(Class)
	 */
	static <T> T getInstance(final Class<T> clazz) throws InstantiationException {
		return InstanceInjector.getInstance(clazz);
	}

	/**
	 * @see fr.isima.dependency.injector.InterfaceInjector
	 *      #interfaceInjection(Class)
	 */
	static <T> T interfaceInjection(final Class<T> clazz) throws ImplementationException, ClassNotFoundException {
		return InterfaceInjector.interfaceInjection(clazz, new ArrayList<>());
	}

	/**
	 * @see fr.isima.dependency.injector.InstanceInjector #classInjection(Class)
	 */
	static <T> T classInjection(final Class<T> clazz)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		return InstanceInjector.classInjection(clazz, new ArrayList<>());
	}

	/**
	 * @see fr.isima.dependency.injector.AbstractInjector #abstractInjection(Class)
	 */
	static <T> T abstractInjection(final Class<T> clazz) throws ImplementationException, ClassNotFoundException {
		return AbstractInjector.abstractInjection(clazz, new ArrayList<>());
	}

	/**
	 * @see fr.isima.dependency.injector.FieldInjector #injectFields(Object, Class)
	 */
	static <T> T injectFields(final T newInstance, final Class<?> clazz2, List<Class<?>> forbidden)
			throws IllegalAccessException, ClassNotFoundException, ImplementationException {
		return FieldInjector.injectFields(newInstance, clazz2, forbidden);
	}

	/**
	 * @see fr.isima.dependency.injector.ConstructorInjector
	 *      #constructorInjection(Class)
	 */
	static <T> T constructorInjection(final Class<T> clazz) throws ImplementationException {
		return ConstructorInjector.constructorInjection(clazz, new ArrayList<>());
	}

	/**
	 * @see fr.isima.dependency.injector.InterfaceInjector
	 *      #injectSpecificInterface(Class, Class)
	 */
	static <T> T injectSpecificInterface(final Class<?> interfazz, final Class<T> clazz)
			throws ClassNotFoundException, InstantiationException {
		return InterfaceInjector.injectSpecificInterface(interfazz, clazz, new ArrayList<>());
	}

}
