package fr.isima.dependency.injector;

import java.util.List;
import java.util.Set;

import org.reflections.Reflections;

import fr.isima.dependency.annotations.Implements;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.utils.Utils;

final class InterfaceInjector {

	private InterfaceInjector() {
	}

	/**
	 * Specific treatment for interface injection. Search for the class implementing
	 * the given interface and inject it. This search is based on annotation
	 * {@link fr.isima.dependency.annotations.Implements @Implements}
	 * 
	 * 
	 * @param clazz
	 *            Interface to process
	 * @return The injected interface
	 * @throws ImplementationException
	 *             No implementation were found for the given interface
	 * @throws ClassNotFoundException
	 *             No annotation found for the given interface
	 */
	@SuppressWarnings("unchecked")
	static <T> T interfaceInjection(final Class<T> clazz, final List<Class<?>> forbidden)
			throws ImplementationException, ClassNotFoundException {
		final Reflections reflections = new Reflections();
		// Gather classes that implements our interface
		final Set<Class<?>> implementers = reflections.getTypesAnnotatedWith(Implements.class, true);
		T foundImplementation = null;
		Class<T> foundClassImplementation = null;

		// get all classes containing Implements annotation
		for (final Class<?> implementer : implementers) {
			// if this class implements clazz
			if (Utils.contains(implementer.getAnnotation(Implements.class).value(), clazz)) {
				if (foundClassImplementation != null)
					throw new ImplementationException(
							"Multiple implementations found for interface " + clazz.getName());
				foundClassImplementation = (Class<T>) implementer; // Inject found implementation and returns it
			}
		}

		if (foundClassImplementation == null)
			throw new ImplementationException("No implementations were found for interface: " + clazz.getName());

		foundImplementation = Injector.inject(foundClassImplementation, forbidden);

		return foundImplementation;
	}

	/**
	 * Use this method in order to specify which class should be used by the
	 * injector when instantiating an interface.
	 * <p>
	 * Interfaces injected inside other classes can not be specifically injected.
	 * 
	 * @param interfazz
	 *            interface to inject
	 * @param clazz
	 *            specific class
	 * @return injected object
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 */
	static <T> T injectSpecificInterface(final Class<?> interfazz, final Class<T> clazz, final List<Class<?>> forbidden)
			throws ClassNotFoundException, InstantiationException {
		if (interfazz.isAssignableFrom(clazz)) {
			return Injector.inject(clazz, forbidden);
		}
		throw new InstantiationException("class " + clazz + "is not an instance of " + interfazz);
	}

}
