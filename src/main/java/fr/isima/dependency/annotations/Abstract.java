package fr.isima.dependency.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Annotation used to specify which class extends which abstract class
 * Specify {@link fr.isima.dependency.annotations.Abstract @Abstract} above class declaration
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Abstract {

	Class<?>[] value();

}
