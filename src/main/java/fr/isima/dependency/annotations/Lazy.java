package fr.isima.dependency.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Annotation used to specify which setter needs to be injected with Lazy Initialization
 * Specify {@link fr.isima.dependency.annotations.Lazy @Lazy} above setter declaration
 *
 * This annotation works but might be dangerous to use.
 * It is much more a proof of concept rather than something you should use.
 * 
 * Use it at your own risks.
 *  
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Lazy {

}
