package fr.isima.dependency.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to specify which constructor needs to be injected during
 * class injection Specify
 * {@link fr.isima.dependency.annotations.InjectField @InjectField} above field
 * declaration
 * <p>
 * Only one constructor per class should be annotated
 *
 */
@Target(ElementType.CONSTRUCTOR)
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectConstructor {

}
