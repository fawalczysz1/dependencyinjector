package fr.isima.dependency.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to specify which field needs to be injected during class
 * injection Specify
 * {@link fr.isima.dependency.annotations.InjectField @InjectField} above field
 * declaration
 * 
 * You can also specify the specific type of implementation to inject when using
 * an interface, using "value" parameter
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectField {

	Class<?> value() default Object.class;

}
