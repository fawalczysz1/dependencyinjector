package fr.isima.dependency.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to specify which setter needs to be injected during class injection
 * Specify {@link fr.isima.dependency.annotations.InjectSetter @InjectSetter} above setter declaration
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectSetter {
	
}
