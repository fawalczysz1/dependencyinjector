package fr.isima.dependency.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to specify which class implements which interface
 * Specify {@link fr.isima.dependency.annotations.Implements @Implements} above class declaration
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Implements{

	Class<?>[] value();

}
