package fr.isima.dependency.lazyinjector;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import fr.isima.dependency.annotations.Lazy;
import fr.isima.dependency.exceptions.ImplementationException;
import fr.isima.dependency.injector.Injector;
import fr.isima.dependency.utils.Utils;

/**
 * Lazy injector class based on
 * {@link fr.isima.dependency.annotations.Lazy @Lazy}. This class uses setter
 * injection at runtime, specifically when a null field needs to be instantiated
 * <p>
 *
 */
public abstract class LazyInjector {

	private LazyInjector() {

	}

	/**
	 * map of methods initialized that contains lazy initializers method
	 */
	private static final Map<String, Method> methodsMap = new HashMap<>();

	/**
	 * This method should be called at class initialization when you want your
	 * fields to be lazy injected. All fields in this class should be called using
	 * class getters.
	 * <p>
	 * As said in {@link fr.isima.dependency.annotations.Lazy @Lazy}, using this
	 * class can be dangerous as it uses a thread per setter annotated, in order of
	 * putting it in a waiting state.
	 * 
	 * @param clazz
	 * @return
	 * @throws ImplementationException
	 * @throws ClassNotFoundException
	 */
	public static <T> T lazyInjectMethod(final Class<T> clazz) throws ImplementationException, ClassNotFoundException {
		T newInstance = null;
		// get clazz instance
		try {
			newInstance = Injector.getInstance(clazz);
		} catch (final InstantiationException e) {
			newInstance = Injector.abstractInjection(clazz);
		}

		for (final Method method : clazz.getDeclaredMethods()) {
			if (method.getAnnotation(Lazy.class) != null) {
				// add method in map
				methodsMap.put(method.toString(), method);
				// create and start waiting on method notify for the given instance
				final Thread t1 = new Thread(new LazyInjector.Waiter(method, newInstance));
				t1.start();
			}
		}
		return newInstance;
	}

	/**
	 * Method called from classes inheriting ILazyInjector
	 * 
	 * @param name
	 *            name of the field to inject
	 * @param instance
	 *            instance from where this method is called
	 * @param fieldType
	 *            return type of the method
	 * @return
	 */
	@SuppressWarnings({ "squid:S2274", "unchecked" })
	public static <T> T get(final String name, final Object instance, final Class<T> fieldType) {
		try {
			final Class<?> clazz = instance.getClass();
			final Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			T result = (T) field.get(instance);
			// if result is null then the field needs to be instantiated
			if (result == null) {

				Method setter = null;
				// loop for all methods in map and for all methods defined in caller class
				for (final Entry<String, Method> entry : methodsMap.entrySet()) {
					for (final Method classMethod : clazz.getMethods()) {
						// if method is defined in map and its name corresponds to the setter naming
						// convention
						if (classMethod.toString().equals(entry.getKey())
								&& classMethod.getName().equalsIgnoreCase("set" + name)) {
							setter = entry.getValue();
						}
					}
				}

				if (setter == null)
					throw new NoSuchMethodException(
							"setter for name : " + name + " in class " + clazz + " is not implemented");
				// notify setter and wait finishing instantiation
				synchronized (setter) {
					setter.notifyAll();
					setter.wait();
				}
				result = (T) field.get(instance);
			}

			return result;

		} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InterruptedException
				| NoSuchMethodException | NoSuchFieldException e) {
			Utils.error(e.toString());
			Thread.currentThread().interrupt();
			return null;
		}

	}

	/**
	 * Thread class used to put the setters in an active waiting state. This thread
	 * will be awaken by {@link fr.isima.dependency.lazyinjector.get get} when
	 * calling notify method
	 * 
	 */
	public static class Waiter implements Runnable {

		Method method;
		boolean stop;
		Object instance;

		public Waiter(final Method method, final Object instance) {
			this.method = method;
			this.instance = instance;
			stop = false;
		}

		/**
		 * Main process overrided from Thread. System based on call notify loop placed
		 * on getters
		 */
		@Override
		public void run() {
			// synchronized block for wait notify methods
			synchronized (method) {
				// looping in case of multiple calls
				while (!stop) {
					try {
						// active wait
						method.wait();
						// get setter argument
						final Class<?> clazz = method.getParameterTypes()[0];
						// invoking setter with instance of object
						method.invoke(instance, Injector.inject(clazz));
						method.notifyAll();

					} catch (InterruptedException | IllegalArgumentException | IllegalAccessException
							| InvocationTargetException | SecurityException | ImplementationException
							| ClassNotFoundException e) {
						stop = true;
						Thread.currentThread().interrupt();
					}
				}
			}
		}
	}
}
