package fr.isima.dependency.lazyinjector;

import fr.isima.dependency.utils.Utils;

/**
 * This interface is designed in order to allow lazy initialization for class
 * parameters
 * 
 * all classes using {@link fr.isima.dependency.annotations.Lazy @Lazy} <b> need
 * </b> to implement this class
 *
 */
public interface ILazyInjector {
	/**
	 * This method is used in order to get value of field passed in parameter.
	 * 
	 * If the field is null, it will be instantiated
	 * 
	 * @param val
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public default <T> T get(final String val) {
		try {
			return (T) LazyInjector.get(val, this, this.getClass().getDeclaredField(val).getType().getClass());
		} catch (NoSuchFieldException | SecurityException e) {
			Utils.error(e.toString());
			return null;
		}
	}

}
