package fr.isima.dependency.exceptions;

public class ImplementationException extends InstantiationException{

	public ImplementationException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5515482518016321017L;

}
