package fr.isima.dependency.utils;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Basic Utils class with logger and table display
 * 
 */
public class Utils {
	private Utils() {

	}

	public static final Logger LOGGER = LoggerFactory.getILoggerFactory().getLogger("logger");

	public static boolean contains(final Object[] objects, final Object obj) {
		return Arrays.asList(objects).contains(obj);
	}

	public static void display(final Object[] objects) {
		for (final Object obj : objects) {
			Utils.debug(obj.toString());
		}
	}

	public static void info(final String str) {
		LOGGER.info(str);
	}

	public static void error(final String str) {
		LOGGER.error(str);
	}

	public static void debug(final String str) {
		LOGGER.debug(str);
	}

	/**
	 * get primitive default value from primitive type
	 * 
	 * @param clazz
	 *            primitive class type
	 * @return default value
	 */
	public static Object getDefaultValue(final Class<?> clazz) {
		if (clazz.isPrimitive()) {
			switch (clazz.getName()) {
			case "int":
				return 0;
			case "float":
				return 0f;
			case "double":
				return 0d;
			case "short":
				return (short) 0;
			case "long":
				return 0l;
			case "byte":
				return (byte) 0;
			case "boolean":
				return false;
			case "char":
				return '\u0000';
			default:
				return null;
			}
		}
		return null;

	}

}
